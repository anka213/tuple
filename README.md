Tuple for Go
============

Tuple is a go type that will hold mixed types / values

	b := Tuple.New(1, 2, "three", 4, "five", 6)
	b.Add("appending")
	i := b.Get(2)  // result is "three"
	f := b.Find("five") // result 4

Possible Future
---------------
 * Add Tuple for multidimensional Tuples
 * Add string keys, as keys are always int right now

Links
-------
 * [Pkg Documentationn](http://go.pkgdoc.org/bitbucket.org/gotamer/tuple "Pkg Documentation")
 * [Repository](https://bitbucket.org/gotamer/tuple "Repository")
 * [Bug Tracking](https://bitbucket.org/gotamer/tuple/issues "Bug Tracking")
 * [Website for your comments](http://www.robotamer.com/html/GoTamer "GoTamer Website")
